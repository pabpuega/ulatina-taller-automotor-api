using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ulatina_taller_automotor_api.Data;
using System;
using System.Linq;
using ulatina_taller_automotor_api.Models;

namespace ulatina_taller_automotor_api.Data
{
    public static class SeedData
    {

        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var _context = new ApiTallerContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ApiTallerContext>>()))
            {
                if (!_context.Vehicles.Any())
                {
                    _context.Vehicles.AddRange(
                        new Vehicle { Plate="AAA-001",Brand = "NISSAN", Model = "LEAF", Year = new DateTime(2020, 01, 01) },
                        new Vehicle { Plate="AAA-002", Brand = "TESLA", Model = "MODEL S", Year = new DateTime(2014, 01, 01) },
                        new Vehicle { Plate="AAA-003", Brand = "FIAT", Model = "500e", Year = new DateTime(2017, 01, 01) }
                    );
                     _context.SaveChanges();
                    _context.Items.AddRange(
                        new Item { PieceName = "Para Choques Frontal", Price = 100 },
                        new Item { PieceName = "Para Choques Trasero", Price = 100 },
                        new Item { PieceName = "Retrovisor Izquierdo", Price = 80 },
                        new Item { PieceName = "Retrovisor Derecho", Price = 80 },
                        new Item { PieceName = "Fibras de Freno", Price = 50 },
                        new Item { PieceName = "Aceite de Moto", Price = 40 },
                        new Item { PieceName = "Aceite de Caja de Cambios", Price = 80 }
                    );
                     _context.SaveChanges();
                    _context.Visits.AddRange(
                        new Visit { VehicleId = 1, VisitDate = DateTime.Parse("2020-01-20"), VisitStatus = VisitStatus.finished, Price = 100 },
                        new Visit { VehicleId = 2, VisitDate = DateTime.Parse("2020-01-10"), VisitStatus = VisitStatus.finished, Price = 150 },
                        new Visit { VehicleId = 3, VisitDate = DateTime.Parse("2020-02-15"), VisitStatus = VisitStatus.finished, Price =  180},
                        new Visit { VehicleId = 3, VisitDate = DateTime.Parse("2020-03-20"), VisitStatus = VisitStatus.finished, Price =  50}
                    );
                     _context.SaveChanges();
                    _context.Services.AddRange(
                        new Service { VisitId = 1, ItemId = 1},
                        new Service { VisitId = 2, ItemId = 1},
                        new Service { VisitId = 2, ItemId = 5},
                        new Service { VisitId = 3, ItemId = 1},
                        new Service { VisitId = 3, ItemId = 3},
                        new Service { VisitId = 4, ItemId = 5}
                    );

                    _context.SaveChanges();
                }

                if (!_context.Users.Any())
                {
                    _context.Users.Add(
                        new ApplicationUser {
                            Id = "8acd1920-d110-4272-8eb4-c04b0d8d2825",
                            UserName = "admin",
                            NormalizedUserName = "ADMIN",
                            PasswordHash = "AQAAAAEAACcQAAAAEJrz2KVZG6xElus70eqvhLO6CVf7XdoW0x3W/JGOJjYuyBpjWNazBvR3+SxsJ4s5pA==",
                            SecurityStamp = "OILQZDTCCBKRNLUDUK53MT4UWY75XUKM",
                            ConcurrencyStamp = "49a68ab9-0ab3-4a63-bc1e-ba387f6ef415",
                            EmailConfirmed = false,
                            PhoneNumberConfirmed = false,
                            TwoFactorEnabled = false,
                            LockoutEnabled = true,
                            AccessFailedCount = 0

                        }
                    );
                    _context.SaveChanges();
                }

            }
        }
    }
}