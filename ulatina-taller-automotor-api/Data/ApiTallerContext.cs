using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ulatina_taller_automotor_api.Models;

namespace ulatina_taller_automotor_api.Data
{
    public class ApiTallerContext : IdentityDbContext<ApplicationUser>
    {
        public ApiTallerContext(DbContextOptions<ApiTallerContext> options)
            : base(options)
        {

        }


        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Service> Services { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Vehicle>().ToTable("Vehicle");
            modelBuilder.Entity<Visit>().ToTable("Visit");
            modelBuilder.Entity<Item>().ToTable("Item");
            modelBuilder.Entity<Service>().ToTable("Service");

            modelBuilder.Entity<Service>()
                .HasKey(s => new { s.VisitId, s.ItemId });
        }

    }
}