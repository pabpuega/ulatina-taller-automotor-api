﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ulatina_taller_automotor_api.Models
{
    public class Service
    {
        public int VisitId { get; set; }
        public int ItemId { get; set; }
        public Item Item { get; set; }
        public Visit Visit { get; set; }
       
    }
}