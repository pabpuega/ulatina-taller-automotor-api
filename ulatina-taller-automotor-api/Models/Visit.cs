using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ulatina_taller_automotor_api.Models
{
    public enum VisitStatus
    {
        scheduled, arrived, inProgress, finished, cancelled
    }
    public class Visit
    {
        public int VisitId { get; set; }
        public int VehicleId { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyyMMdd}")]
        public DateTime VisitDate { get; set; }
        public Vehicle Vehicle { get; set; }
        public VisitStatus VisitStatus { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }
         public List<Service> Services { get; set; }
    }
}