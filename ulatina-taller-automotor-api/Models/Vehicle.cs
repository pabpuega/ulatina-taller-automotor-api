using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ulatina_taller_automotor_api.Models
{
    public class Vehicle
    {
        public int VehicleId { get; set; }
        [Display(Name = "Placa")]
        public string Plate { get; set; }
        [Display(Name = "Marca")]
        public string Brand { get; set; }
        [Display(Name = "Modelo")]
        public string Model { get; set; }
        [Display(Name = "Año")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy}")]
        public DateTime Year { get; set; }
        public List<Visit> Visits { get; set; }
    }
}