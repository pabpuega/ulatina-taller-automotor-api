﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ulatina_taller_automotor_api.Models
{
    public class Item
    {
       public int ItemId { get; set; }
        
        [Display(Name = "Nombre")]
        public string PieceName { get; set; }
        [Display(Name = "Modelo")]
        public string PieceModel { get; set; }
        [Display(Name = "Price")]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }
        public List<Service> Services { get; set; }

    }
}