﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ulatina_taller_automotor_api.Data;
using ulatina_taller_automotor_api.Models;

namespace ulatina_taller_automotor_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class VisitsController : ControllerBase
    {
        private readonly ApiTallerContext _context;

        public VisitsController(ApiTallerContext context)
        {
            _context = context;
        }

        // GET: api/Visits
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Visit>>> GetVisits()
        {
            return await _context.Visits.ToListAsync();
        }

        // GET: api/Visits/Plate/5B
        [HttpGet("Plate/{plate}")]
        public async Task<ActionResult<IEnumerable<Visit>>> GetVisitsPlate(string plate)
        {
            var vehicle = await _context.Vehicles.FirstOrDefaultAsync(v => v.Plate.ToUpper() == plate.ToUpper());

            if (vehicle == null)
            {
                return new List<Visit>();
            }

            var visits = _context.Visits.Where(v => v.VehicleId == vehicle.VehicleId);

            return await visits.ToListAsync();
        }

        // GET: api/Visits/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Visit>> GetVisit(int id)
        {
            var visit = await _context.Visits
                .Include(v => v.Services)
                .ThenInclude(s => s.Item)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.VisitId == id);

            if (visit == null)
            {
                return NotFound();
            }

            return visit;
        }

        // GET: api/Visits/5
        [HttpGet("{visitId}/{itemId}")]
        public async Task<ActionResult<Visit>> GetVisitItem(int visitId, int itemId)
        {
            //var visit = await _context.Visits.FindAsync(visitId);
            var visit = await _context.Visits
                .Include(v => v.Services)
                .ThenInclude(s => s.Item)
                .AsNoTracking()
                .FirstOrDefaultAsync(v => v.VisitId == visitId && v.Services.FirstOrDefault(s => s.Item.ItemId == itemId).Item.ItemId == itemId);

            if (visit == null)
            {
                return NotFound();
            }

            return visit;
        }

        // DELETE: api/Visits/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Visit>> DeleteVisit(int id)
        {
            var visit = await _context.Visits.FindAsync(id);

            if (visit == null)
            {
                return NotFound();
            }

            _context.Visits.Remove(visit);
            await _context.SaveChangesAsync();

            return visit;
        }

        // PUT: api/Visits/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVisit(int id, Visit visit)
        {
            if (id != visit.VisitId)
            {
                return BadRequest();
            }

            _context.Entry(visit).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VisitExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Visits
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Visit>> PostVisit(Visit visit)
        {
            _context.Visits.Add(visit);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVisit", new { id = visit.VisitId }, visit);
        }

        // POST: api/Visits/5/1
        [HttpPost("{visitId}/{itemId}")]
        public async Task<ActionResult<Visit>> PostVisitItem(int visitId, int itemId)
        {
            var visit = await _context.Visits.FindAsync(visitId);
            if (visit == null)
            {
                return NotFound();
            }

            var item = await _context.Items.FindAsync(itemId);
            if (item == null)
            {
                return NotFound();
            }


            _context.Services.Add(new Service{VisitId = visit.VisitId, ItemId = item.ItemId});
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVisitItem", new { visitId = visit.VisitId, itemId = item.ItemId }, visit);
        }

        // DELETE: api/Visits/5/1
        [HttpDelete("{visitId}/{itemId}")]
        public async Task<ActionResult<Service>> DeleteVisitItem(int visitId, int itemId)
        {
            var service = await _context.Services.FirstOrDefaultAsync(s => s.VisitId == visitId && s.ItemId == itemId);
            if (service == null)
            {
                return NotFound();
            }

            _context.Services.Remove(service);
            await _context.SaveChangesAsync();

            return service;
        }

        private bool VisitExists(int id)
        {
            return _context.Visits.Any(e => e.VisitId == id);
        }
    }
}
