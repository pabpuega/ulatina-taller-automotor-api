# API TALLER
## Acerca de
Universidad Latina
Proyecto Progra Avanzada
Integrantes: Andres & Andres

## Preconfiguraci�n

1. Clonar el proyecto con git: 
```bash
git clone https://bitbucket.org/pabpuega/ulatina-taller-automotor-api.git
```

2. Abrir una linea de comandos, navegar hasta el folder y aplicar los siguientes comandos:
```bash
git switch Develop
cd ulatina-taller-automotor-api
dotnet restore
dotnet tool install --global dotnet-ef
dotnet ef database update
dotnet run
```

## HTTP Verbs


------------

### LOGIN

------------

### Sin Authorization

-----------


|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Account/login | POST  | https://localhost:5000/api/account/login  |

Body:
```json
{
	"userName": "admin",
	"password": "admin"
}
```
Response:
- 200: OK
```json
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluIiwicm9sIjoiQWRtaW5pc3RyYWRvciIsImp0aSI6ImQ0ZTAwYmYxLWNmMDMtNDM5ZS05NDc3LWY0OTkwYjRjNGYxZiIsImV4cCI6MTU4NjIwODQ4MiwiaXNzIjoidGFsbGVyLmxvY2FsIiwiYXVkIjoidGFsbGVyLmxvY2FsIn0.Dmt_oQteIiCjnqkh21z8Mo98k69_ISraOA_yanZ33S4",
    "expiration": "2020-04-06T21:28:02.8040161Z"
}
```


------------

### Con Authorization

-----------

Headers:

| Key  | Value  |
| ------------ | ------------ |
|Authorization|Bearer *tokenValueHere*|
|Content-Type|application/json|

-----------

|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Account/create | POST  | https://localhost:5000/api/account/create  |

Body:
```json
{
	"userName": "admin",
	"password": "admin"
}
```
Response:
- 200: OK
```json
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluIiwicm9sIjoiQWRtaW5pc3RyYWRvciIsImp0aSI6ImQ0ZTAwYmYxLWNmMDMtNDM5ZS05NDc3LWY0OTkwYjRjNGYxZiIsImV4cCI6MTU4NjIwODQ4MiwiaXNzIjoidGFsbGVyLmxvY2FsIiwiYXVkIjoidGFsbGVyLmxvY2FsIn0.Dmt_oQteIiCjnqkh21z8Mo98k69_ISraOA_yanZ33S4",
    "expiration": "2020-04-06T21:28:02.8040161Z"
}
```


------------


|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Vehicles | GET  | https://localhost:5000/api/vehicles  |

Body:
```json
{

}
```
Response:
- 200: OK
```json
[
    {
        "vehicleId": 1,
        "plate": null,
        "brand": "NISSAN",
        "model": "LEAF",
        "year": "2020-01-01T00:00:00",
        "visits": null
    },
    {
        "vehicleId": 2,
        "plate": null,
        "brand": "TESLA",
        "model": "MODEL S",
        "year": "2014-01-01T00:00:00",
        "visits": null
    },
    {
        "vehicleId": 3,
        "plate": null,
        "brand": "FIAT",
        "model": "500e",
        "year": "2017-01-01T00:00:00",
        "visits": null
    }
]
```
----------
|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Vehicles/id | GET  | https://localhost:5000/api/vehicles/{id}  |

Body:
```json
{

}
```
Response:
- 200: OK
```json
{
    "vehicleId": 1,
    "plate": null,
    "brand": "NISSAN",
    "model": "LEAF",
    "year": "2020-01-01T00:00:00",
    "visits": [
        {
            "visitId": 4,
            "vehicleId": 1,
            "visitDate": "2020-01-20T00:00:00",
            "visitStatus": 3,
            "price": 100.0,
            "services": null
        }
    ]
}
```
----------
|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Vehicles/id | PUT  | https://localhost:5000/api/vehicles/{id}  |

Body:
```json
{
	"vehicleId": 1,
    "plate": "274612",
    "brand": "NISSAN",
    "model": "LEAF",
    "year": "2020-01-01T00:00:00"
}
```
Response:
- 204: Success No Content

----------
|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Vehicles | POST  | https://localhost:5000/api/vehicles  |
Body:
```json
{
{
	"vehicleId": 5,
    "plate": "274612",
    "brand": "NISSAN",
    "model": "LEAF",
    "year": "2020-01-01T00:00:00"
}
}
```
Response:
- 201: Created
```json
{
	"vehicleId": 5,
    "plate": "274612",
    "brand": "NISSAN",
    "model": "LEAF",
    "year": "2020-01-01T00:00:00"
}
```
----------

|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Vehicles/id | DELETE  | https://localhost:5000/api/vehicles/{id}  |

Body:
```json
{
}
```
Response:
- 200: OK
```json
{
     "vehicleId": 6,
    "plate": "274612",
    "brand": "NISSAN",
    "model": "LEAF",
    "year": "2020-01-01T00:00:00"
}
```

----------
|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Items | GET  | http://localhost:5000/api/items  |

Body:
```json
{
}
```
Response:
- 200: Ok
```json
[
   {
        "itemId": 1,
        "pieceName": "Retrovisor Izquierdo",
        "pieceModel": null,
        "price": 80.0,
        "services": null
    },
    {
        "itemId": 2,
        "pieceName": "Para Choques Trasero",
        "pieceModel": null,
        "price": 100.0,
        "services": null
    },
    {
        "itemId": 3,
        "pieceName": "Para Choques Frontal",
        "pieceModel": null,
        "price": 100.0,
        "services": null
    },
    {
        "itemId": 4,
        "pieceName": "Retrovisor Derecho",
        "pieceModel": null,
        "price": 80.0,
        "services": null
    },
    {
        "itemId": 5,
        "pieceName": "Fibras de Freno",
        "pieceModel": null,
        "price": 50.0,
        "services": null
    },
    {
        "itemId": 6,
        "pieceName": "Aceite de Moto",
        "pieceModel": null,
        "price": 40.0,
        "services": null
    },
    {
        "itemId": 7,
        "pieceName": "Aceite de Caja de Cambios",
        "pieceModel": null,
        "price": 80.0,
        "services": null
    }
]
```

----------
|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Items/id | GET  | http://localhost:5000/api/items/{id}  |

Body:
```json
{
}
```
Response:
- 200: Ok
```json
{
	"itemId": 2,
    "pieceName": "Para Choques Trasero",
    "pieceModel": null,
    "price": 100.0,
}
```
----------
|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Items/id | PUT  | http://localhost:5000/api/items/{id}  |

Body:
```json
{
	"itemId": 2,
    "pieceName": "Para Choques Trasero",
    "pieceModel": null,
    "price": 200.0
}
```
Response:
- 204: Success No Content

----------

|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Items | POST  | http://localhost:5000/api/items  |

Body:
```json
{
	"ItemId": 10 ,
    "PieceName": "Bumper",
    "PieceModel": "t1-n2" ,
    "Price": 200.0
}
```
Response:
- 201: Created
```json
{
    "ItemId": 10,
    "PieceName": "Bumper",
    "PieceModel": "t1-n2",
    "Price": 200.0
}
```
------------

|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Items/id | DELETE  | http://localhost:5000/api/items/{id}  |

Body:
```json
{

}
```
Response:
- 200: Ok
```json
{
	"itemId": 10,
    "pieceName": "Bumper",
    "pieceModel": "t1-n2",
    "price": 200.0
}
```
---------

|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Visits | GET  | http://localhost:5000/api/Visits  |

Body:
```json
{

}
```
Response:
- 200: Ok
```json
[
    {
        "visitId": 1,
        "vehicleId": 3,
        "visitDate": "2020-03-20T00:00:00",
        "vehicle": null,
        "visitStatus": 3,
        "price": 50.0,
        "services": null
    },
    {
        "visitId": 2,
        "vehicleId": 3,
        "visitDate": "2020-02-15T00:00:00",
        "vehicle": null,
        "visitStatus": 3,
        "price": 180.0,
        "services": null
    },
    {
        "visitId": 3,
        "vehicleId": 2,
        "visitDate": "2020-01-10T00:00:00",
        "vehicle": null,
        "visitStatus": 3,
        "price": 150.0,
        "services": null
    },
    {
        "visitId": 4,
        "vehicleId": 1,
        "visitDate": "2020-01-20T00:00:00",
        "vehicle": null,
        "visitStatus": 3,
        "price": 100.0,
        "services": null
    }
]
```
-------
|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Visits/Plate | GET  | http://localhost:5000/api/Visits/Plate/{plate} |

Body:
```json
{

}
```
Response:
- 200: Ok
```json
[
    {
        "visitId": 4,
        "vehicleId": 1,
        "visitDate": "2020-01-20T00:00:00",
        "vehicle": {
            "vehicleId": 1,
            "plate": "274612",
            "brand": "NISSAN",
            "model": "LEAF",
            "year": "2020-01-01T00:00:00",
            "visits": []
        },
        "visitStatus": 3,
        "price": 100.0,
        "services": null
    }
]
```
---------

|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Visits/id | GET  | http://localhost:5000/api/Visits/{id} |

Body:
```json
{

}
```
Response:
- 200: Ok
```json
{
    "visitId": 3,
    "vehicleId": 2,
    "visitDate": "2020-01-10T00:00:00",
    "vehicle": null,
    "visitStatus": 3,
    "price": 150.0,
    "services": [
        {
            "visitId": 3,
            "itemId": 1,
            "item": {
                "itemId": 1,
                "pieceName": "Retrovisor Izquierdo",
                "pieceModel": null,
                "price": 80.0,
                "services": []
            }
        },
        {
            "visitId": 3,
            "itemId": 3,
            "item": {
                "itemId": 3,
                "pieceName": "Para Choques Frontal",
                "pieceModel": null,
                "price": 100.0,
                "services": []
            }
        }
    ]
}
```
-----
|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Visits/visitID/itemID | GET  | http://localhost:5000/api/Visits/{VisitID}/{itemid} |

Body:
```json
{

}
```
Response:
- 200: Ok
```json
{
    "visitId": 3,
    "vehicleId": 2,
    "visitDate": "2020-01-10T00:00:00",
    "vehicle": null,
    "visitStatus": 3,
    "price": 150.0,
    "services": [
        {
            "visitId": 3,
            "itemId": 1,
            "item": {
                "itemId": 1,
                "pieceName": "Retrovisor Izquierdo",
                "pieceModel": null,
                "price": 80.0,
                "services": []
            }
        },
        {
            "visitId": 3,
            "itemId": 3,
            "item": {
                "itemId": 3,
                "pieceName": "Para Choques Frontal",
                "pieceModel": null,
                "price": 100.0,
                "services": []
            }
        }
    ]
}
```
---------

|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Visits/id | PUT  | http://localhost:5000/api/Visits/{id} |

Body:
```json
{
	"visitId": 2,
    "vehicleId": 2,
    "visitDate": "2020-01-10T00:00:00",
    "vehicle": null,
    "visitStatus": 3,
    "price": 300.0,
    "services": null
}
```
Response:
- 200: Ok
```json
{
}
```

|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Visits/visitID/itemID | POST  | http://localhost:5000/api/Visits/{VisitID}/{itemid} |

Body:
```json
{
}
```
Response:
- 200: Ok
```json
{
    "visitId": 2,
    "vehicleId": 3,
    "visitDate": "2020-02-15T00:00:00",
    "vehicle": null,
    "visitStatus": 3,
    "price": 180.0,
    "services": [
        {
            "visitId": 2,
            "itemId": 3,
            "item": {
                "itemId": 3,
                "pieceName": "Para Choques Frontal",
                "pieceModel": null,
                "price": 100.0,
                "services": []
            }
        }
    ]
}
```
----------

|  PATH |METHOD | EXAMPLE  |
| ------------ | ------------ | ------------ |
| /api/Visits/visitID/itemID | DELETE  | http://localhost:5000/api/Visits/{VisitID}/{itemid} |

Body:
```json
{

}
```
Response:
- 200: Ok
```json
{
    "visitId": 2,
    "itemId": 3,
    "item": null,
    "visit": null
}
```
